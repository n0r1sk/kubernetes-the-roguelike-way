# Kubernetes the roguelike way
Build up Kubernetes the roguelike way on premise. No scripts. No clouds. No magic.

![Moria Hometown](img/303c4c.jpg)

### Idea
This documentation project is highly inspired by [Kelsey Hightower's - Kubernetes the hard way](https://github.com/kelseyhightower/kubernetes-the-hard-way) but focuses the Kubernetes on premise installation, configuration and usage.

### Why
Not everyone is using a cloud platform (or multiple cloud platforms) and there are different reasons why they not use them. Sometime you already have multiple data centers on premise and still want to use them, like in our case. Kubernetes and other cloud native software was born on the internet and most of the time it is not easy to take and integrate them on premise. But if you want them or need them, you may have no other choice as to deep dive into the components and find out how the things are really working. And while doing so, you can be sure, that you will face a lot of pitfalls where you have to start over and over again. I compare this with a special kind of computer games which are commonly know as **Roguelike Games**. If you make a terrible mistake, you face the permadeath and have to start over - but you every time you try you gain experience and get better in understanding the game.

### What
This documentations will cover a lot of details but it is not meant to be a full step-by-step guide. It is also not meant to be complete. Things are changing over the time. Please, if you find changes or parts which are not working an more, open an issue or contribute a change! Thanks!

As in every roguelike we [start in town](img/303c4c.jpg). Here is the overview about the core topics.

- [Operating System (depth 50 feet)](docs/fd92da.md)
  - [Ubuntu](docs/ae4f31.md)
  - [Docker](docs/dde8f6.md)
  - [Certificates](docs/1b2122.md)
  - [ETCD](docs/9e5a36.md)
  - [CoreDNS](docs/61a69f.md)
- [Kubernetes cluster with kubeadm on-premise](docs/82af2c.md)
  - [Installing and configuring keepalived](docs/73ce57.md)
  - [Installing the Kuberntes components](docs/2f7201.md)
  - [Bootstrapping the masters with kubeadm](docs/8ca563.md)
- [Calico network plugin](docs/c2ec56.md)
  - [BGP routing, Linux router with BGP route reflector](docs/d3558e.md)
  - [Custom installation, Kubernetes node labels](docs/497c95.md)
  - [Multiple datacenter / Hybrid cloud / Change existing configuration/ ip pool per node](docs/9790cf.md)
- [Kubernetes nodes](docs/9ba035.md)
  - [Join master](docs/0ddd65.md)
  - [Labels](docs/1f8d6b.md)
- [Kubernetes dashboard](docs/8e11df.md)
  - [Installation / RBAC](docs/7aa9c8.md)
  - [Gitlab authentication](docs/ec9a61.md)
  - [kube-bosnd Ingress load balancer / CoreDNS](docs/7e5f44.md)
  - [Isolate Kubernetes dashboard with Calico network policies](docs/1634cf.md)
 
### The big picture
![The big picture](img/482c79.png)
