![CC BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

The work in this repository is licensed under CC BY-SA.

Read the full license text by following this url: https://creativecommons.org/licenses/by-sa/4.0/legalcode
