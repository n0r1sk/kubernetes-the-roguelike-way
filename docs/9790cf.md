# Multiple datacenter / Hybrid cloud / Change existing configuration/ ip pool per node

Kubernetes is able to [manage thousends of nodes and tens of thousends pods (and much more containers)](https://kubernetes.io/docs/admin/cluster-large/). But like it or not, sometimes it is necessary to span a cluster across multiple data center locations because there are some benefits if it is still one Kubernetes cluster:
- Ingress and Egress policies are only enforceable in the same cluster
- Deployments to one cluster are easier than to multiple clusters
- Kubernetes includes a lot of features which are supporting large cluster setups like namespaces, RBAC and so on

This documentation reflects a challenging task. It might break something (eg workloads) at some points. This is like quaffing a potion which is not identified in a roguelike game - strange things may happen.

## Multiple datacenter

The [Calico/node](https://docs.projectcalico.org/v3.1/reference/architecture/components) has to be configured to be usable in a multi data center environment. During the setup of the Kubernetes cluster with kubeadm, you have specified the `--pod-network-cidr string` according to the environment. This CIDR should be large enough to cover the network you would like to use for the multiple data center networks. Otherwise `kube-proxy` might not work.

~~~
192.168.160.0/24      first network to use (data center 1)
192.168.161.0/25      second network to use (data center 2)
~~~

Details about how to change the configuration can be found in the next section `Change existing configuration`. `kubeadm` can only use one CIDR at the moment which are setup during the `kubeadm` init run.

Therefore, if Calico is installed for the first time, you might not be able to create all necessary configurations for Calico beforehand. It might be easier to change the initial configuration after the first installation.

## Install calico

Calico can be installed by following the [official documentation](https://docs.projectcalico.org/v3.1/getting-started/kubernetes/installation/calico). First apply the `RBAC` rules and afterwards **download** the Calico config.yml. **Do not apply the config.yml directly!** With the use of external ETCD, the config.yml **must** be changed to inlcude the ETCD certificates before the installation can proceed!

### Changes to calico.yml

Changes needed to support external ETCD:

- etcd_endpoints: "https://10.x.X.50:2379,https://10.x.x.51:2379,https://10.x.x.52:2379,"
- etcd_ca: "/calico-secrets/etcd-ca"
- etcd_cert: "/calico-secrets/etcd-cert"
- etcd_key: "/calico-secrets/etcd-key"

And the real certificates must be BASE64 encoded stored in the config map. To create the BASE64 encoding use the following command. For details see [Kubernetes secrets](https://kubernetes.io/docs/concepts/configuration/secret/#overview-of-secrets)
~~~bash
cat etcd_key | base64 -w 0
~~~

~~~
etcd-key: LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFcEFJQkFBS0NBUUVBdWtpbG5SUk1SL1pjL0w5ZUJ0WEpEcjN6aEZOYWtleURPalJRSE5EYmdMMlhpOVFmCmFmNDFDUHR3bStNKy9nSjNadUZzNlNWVmxERGlZSHNwNXBLYlpUdEJKODZnWVRqQ1pQZUcxR2tzdjN0cnZaVGkKMjhaMy9JaGowY201MjlDbG1KY0FkZkt4VVVxSWQ3OW9VVFdtTUQ4bE5xMi9PdEZCSTI5aS9lemxTNmVRa0hPWAppV0llWU5WR1BXdm5IaEFBUnpqMjAwdzVkZVZvNUZyCkdPQXZzRlhwWFpmN2FSbmlpeGl6Y1FLQmdRQ2ZZVEpNWVV5eHc1blMwWDUzQ1lPYk1CV2tUOEoyTXdkWW5XRHcKTW9FWWJXRVJ1K2plcmtIdUpIdUE0RTJJeDhhbld2NThtenFjOEpDeGJ5NzJQWkg5K0x0Q0t2SkJNc05CVGFQKwp3ZDhnRnhPaHBJNVVXVTVsV01DbHFVeGk3Q003aDBrVmZ0eFBTWEJZQTMxbkJ6OXBVSU94ZnpUNTlPaWVGbElZCnhZOFQyUUtCZ1FEUlRqcyszWTNLa01rK1IyOWprMmtyWGw2Rnp0R2k5cFJKSVdBZ2dhdTh4WUNWUURkZEJRYzgKeTA0S1ZNbTUwSEMzdW1uNHFPdWRhdVMrNTJXR21hemQ3L0xTUWhYanlXemdDdnZXaExQazJsT1lCZk5QbHpqeQo3QU02TVRaeTg0NlRtb0dRQkRwdkVydGZDSmY2dnRrTCtWTlkzVjR4RW5jVldpc1oyYUdGSGc9PQotLS0tLUVORCBSU0EgUFJJVkFURSBLRVktLS0tLQo=
~~~

After that, install calico with:

~~~bash
kubectl apply -f calico.yaml
~~~

Check if the Calico ETCD keys were written:
~~~bash
docker run --rm \
  -v /etc/kubernetes/pki/etcd:/certs \
  -e ETCDCTL_API=3 \
  quay.io/coreos/etcd:v3.3.2 \
  etcdctl \
    --cacert=/certs/ca.pem \
    --cert=/certs/etcd-client.pem \
    --key=/certs/etcd-client-key.pem \
    --endpoints=10.x.x.50:2379 get /calico --prefix --keys-only
~~~

## Install calicoctl

For a lot of commands, the `calicoclt` binary is needed. Use the [official documentation](https://docs.projectcalico.org/v3.1/usage/calicoctl/install) to download and install the `calicoctl` binary.

If the Kubernetes cluster is configured to use external ETCD's, be sure to create a configuraiont file for calico, as it would not be possible to manage you calico without them.

`calicoctl` needs a configuration file, as otherwise every parameter must be given as command line switch. Place the configuration file unter `\etc\calico\calicoctl.cfg`. This path and filename will be used by `calicoctl` by default.

~~~yaml
apiVersion: projectcalico.org/v3
kind: CalicoAPIConfig
metadata:
spec:
  etcdEndpoints: https://10.x.x.50:2379,https://10.200.x.x:2379,https://10.200.x.x:2379
  etcdKeyFile: /etc/kubernetes/pki/etcd/etcd-client-key.pem
  etcdCertFile: /etc/kubernetes/pki/etcd/etcd-client.pem
  etcdCACertFile: /etc/kubernetes/pki/etcd/ca.pem
~~~

Afterward check if you can use `calicoclt` by running the following command:
~~~bash
calicoctl get no
~~~

## Change existing configuration

Changing the running configuration might be the only way on how to create a multi data center setup with Calico.

![Calico/node IPAM config](../img/d66247.png)

### Check list
- [ ] Add/Change the new ip pool to the Calico configuration
- [ ] Add/Change the Kubernetes Calico config map to reflect the new ip pool
- [ ] Add/Change the Kubernetes Calico node daemonset to reflect the new ip pool
- [ ] Remove the CIDR configuration of kube-proxy and kube-controller
- [ ] Disable node to node full mesh BGP peering
- [ ] Enable per node BGP peering
- [ ] Change the local AS number for a node
  
### Add/Change the new ip pool to the Calico configuration

The [Calico documentation on changing ip pools](https://docs.projectcalico.org/v3.1/usage/changing-ip-pools) lists at least three points where the ip pool needs to be changed.

- kube-apiserver: `--pod-network-cidr`
- kube-proxy: `--cluster-cidr`
- kube-controller-manager: `--cluster-cidr`

In the first step check which workloads have already assigned an ip address of the current CIDR. Run the command `./calicoctl get workloadEndpoint --all-namespaces`. This will produce the following example output. We have to reload this services later to get ip addresses from the new ip pool.

~~~
NAMESPACE     WORKLOAD                            NODE           NETWORKS             INTERFACE         
kube-system   kube-dns-86f4d74b45-zpzvq           x207           192.168.129.129/32   cali2f26fb21b25   
nginx-test    nginx-deployment-6c575c8b98-fz4kk   x208           192.168.144.150/32   cali0f22577f8e5   
nginx-test    nginx-deployment-6c575c8b98-xnv46   x208           192.168.144.149/32   calic9991bf306d   
nginx-test    ctm-lb-86dfb675ff-hnzck             x209           192.168.154.211/32   cali70407030a8e   
nginx-test    nginx-deployment-6c575c8b98-bnnzv   x209           192.168.154.208/32   calia6c8b405957   
nginx-test    nginx-deployment-6c575c8b98-qmvzz   x209           192.168.154.207/32   calide9371a3025 
~~~

To show which pool is currently in use run the command `./calicoctl get ipPool -o wide`:
~~~
NAME                  CIDR               NAT    IPIPMODE   DISABLED   
default-ipv4-ippool   192.168.128.0/19   true   Always     false
~~~

Add a new pool to the Calico ip pools by running the following command.

~~~bash
./calicoctl create -f -<<EOF
apiVersion: projectcalico.org/v3
kind: IPPool
metadata:
  name: pool-at-1
spec:
  cidr: 192.168.160.0/24
  ipipMode: Always
  natOutgoing: true
EOF

./calicoctl create -f -<<EOF
apiVersion: projectcalico.org/v3
kind: IPPool
metadata:
  name: pool-de-1
spec:
  cidr: 192.168.161.0/25
  ipipMode: Always
  natOutgoing: true
EOF
~~~

Now the new pool should be shown up by `./calicoctl get ipPool -o wide`:
~~~
NAME                  CIDR               NAT    IPIPMODE   DISABLED   
default-ipv4-ippool   192.168.128.0/19   true   Always     false      
pool-at-1             192.168.160.0/24   true   Always     false 
~~~

*NOTICE*: If you get the waring ` calicoctl apply -f calico-ip-pool-at-1.yml
Failed to apply 'IPPool' resource: error with field IPPool.Spec.CIDR = '192.168.128.0/19' (IPPool(pool-at-1) CIDR overlaps with IPPool(default-ipv4-ippool) CIDR 192.168.0.0/16)` then you have to delete the default pool before adding the new one. The default pool is created during the setup and reflects the Kubernetes kube-proxy configuration.

To disable the old ip pool first export the running configuration yaml to a file with the command `calicoctl get ippool -o yaml > calico-pool.yaml`. The edit the yaml file and add `disabled: true` to the the correct section. Afterwards the file should look like this (remove the metadata before you apply the config):

~~~yaml
apiVersion: projectcalico.org/v3
kind: IPPoolList
items:
- apiVersion: projectcalico.org/v3
  kind: IPPool
  metadata:
    name: default-ipv4-ippool
  spec:
    disabled: true
    cidr: 192.168.128.0/19
    ipipMode: Always
    natOutgoing: true
- apiVersion: projectcalico.org/v3
  kind: IPPool
  metadata:
    name: pool-at-1
  spec:
    cidr: 192.168.160.0/24
    ipipMode: Always
    natOutgoing: true
~~~

*NOTE*: Comment some lines as otherwise it would not be possible to apply the new yaml file because of the following error: `Failed to apply any 'IPPool' resources: etcdserver: mvcc: required revision has been compacted`

Apply the edited file with `calicoctl apply -f calico-pool.yaml`. And the pool should be disabled now:

~~~bash
./calicoctl get ippool -o wide
NAME                  CIDR               NAT    IPIPMODE   DISABLED   
default-ipv4-ippool   192.168.128.0/19   true   Always     true       
pool-at-1             192.168.160.0/24   true   Always     false    
~~~

*NOTICE*: If you have to move the ip pool to another cluster, you **must** delete the disabled pool, as it is still used in the BGP configuration. [See this issue for details](https://github.com/projectcalico/calico/issues/2064)

After the changes are done, delete one of the workload and recreate it. The workload should come online with an ip address from the new ip pool.

Also the BGP router should alredy got the new routes from the Calico BGP peers:
~~~
bird> show route 
...
192.168.160.0/26   via 10.x.x.208 on ens160 [atlxweb26208 16:06:55] * (100) [i]
...
~~~

### Add/Change the Kubernetes Calico config map to reflect the new ip pool

The [Calico/node](https://docs.projectcalico.org/v3.1/reference/architecture/components) is the core component of the the Calico installation. The Calico/node manages all the stuff that needs to happen on your Kubernetes node, BGP management, IPAM management and so on.

To use different ip pools on different Kubernetes/Calico nodes change the file `/etc/cni/net.d/10-calico.conflist`. This file is written/overwritten by the `install-cni` container which part of the `calico-node` pods. Use `kubectl -n kube-system describe po calico-node-5tlfx` on an existing `calico-node` pod to see the configuration.

The `install-cni` container uses the environment variable `CNI_NETWORK_CONFIG:  <set to the key 'cni_network_config' of config map 'calico-config'>  Optional: false` to retrieve the IPAM configuration which is stored in the config map `calico-config` with the key `cni_network_config`.

It is a good practice to append your cni configuration to the yml file instead of editing the default one (` kubectl -n kube-system get configmaps calico-config -o yaml > calico-config-map.yml`. We will use the name `cni_network_config_a` later inside the daemon set to apply the config only to a set of Kubernetes nodes which are labeled in a certain manner. Apply the modified config map with `kubectl apply -f calico-config-map.yml`.

~~~yaml
  cni_network_config_a: |-
    {
      "name": "k8s-pod-network",
      "cniVersion": "0.3.0",
      "plugins": [
        {
          "type": "calico",
          "etcd_endpoints": "__ETCD_ENDPOINTS__",
          "etcd_key_file": "__ETCD_KEY_FILE__",
          "etcd_cert_file": "__ETCD_CERT_FILE__",
          "etcd_ca_cert_file": "__ETCD_CA_CERT_FILE__",
          "log_level": "info",
          "mtu": 1500,
          "ipam": {
              "type": "calico-ipam",
              "assign_ipv4": "true",
              "ipv4_pools": ["192.168.161.0/24"]
          },
          "policy": {
              "type": "k8s"
          },
          "kubernetes": {
              "kubeconfig": "__KUBECONFIG_FILEPATH__"
          }
        },
        {
          "type": "portmap",
          "snat": true,
          "capabilities": {"portMappings": true}
        }
      ]
    }

~~~

### Add/Change the Kubernetes Calico node daemonset to reflect the new ip pool

In addition, Calico/node pods are created via a Kubernetes daemonset. Therefore the configuration of which key (`cni_network_config`) of which config map (`calico-config`) should be used for the IPAM configuration can be changed there! 

First list the actual daemon set. The Calico daemonset has no node selector specified.

~~~bash
kubectl -n kube-system get daemonset
NAME          DESIRED   CURRENT   READY     UP-TO-DATE   AVAILABLE   NODE SELECTOR                   AGE
calico-node   3         3         3         3            3           <none>                          3d
kube-proxy    3         3         3         3            3           beta.kubernetes.io/arch=amd64   3d
~~~

Save the Calico daemonset an edit it to reflect the config-map setting specified eralier and specify that this daemonset should only run on specific nodes. (all in the container spec)
~~~yaml
        - name: CNI_NETWORK_CONFIG
          valueFrom:
            configMapKeyRef:
              key: cni_network_config_at
              name: calico-config

      dnsPolicy: ClusterFirst
      hostNetwork: true
      nodeSelector:
        dc: <yourlabel>
~~~

Now, delete the exisiting daemonset and add your own one.

~~~
kubectl -n kube-system delete daemonset calico-node
kubectl -n kube-system apply -f calico-daemonset-at.yml
kubectl -n kube-system get daemonset
NAME          DESIRED   CURRENT   READY     UP-TO-DATE   AVAILABLE   NODE SELECTOR                   AGE
calico-node   3         3         3         3            3           dc=<yourlabel>                  1m
kube-proxy    3         3         3         3            3           beta.kubernetes.io/arch=amd64   3d
~~~

Now, remove a already existing workload, eg coredns (Kubernetes v1.11) and after recreation, the pod should come back online with an ip address of the specified pool!

~~~bash
kubectl -n kube-system get po -o wide | grep coredns
coredns-78fcdf6894-fshm2                  1/1       Running   0          3d        192.168.120.2   x462
coredns-78fcdf6894-mhhh8                  1/1       Running   0          3d        192.168.120.1   x462
kubectl -n kube-system delete po coredns-78fcdf6894-fshm2
pod "coredns-78fcdf6894-fshm2" deleted
kubectl -n kube-system get po -o wide | grep coredns
coredns-78fcdf6894-pscjq                  1/1       Running   0          7s        192.168.156.1   atlxkube461
coredns-78fcdf6894-xgtv2                  1/1       Running   0          57s       192.168.151.1   atlxkube463
~~~

### Remove the CIDR configuration of kube-proxy and kube-controller

This is just an information. https://github.com/projectcalico/calico/issues/683 Currently during the kubeadm setup you have to specify a network segment which is large enough to cover all your other networks.

### Disable node to node full mesh BGP peering

Run the following commang to save the current configuration. Details can be found [here](https://docs.projectcalico.org/v3.1/usage/configuration/bgp).
~~~
./calicoctl get bgpconfig default -o yaml > calico-global-peer.yaml
~~~

Edit the file and disable the node to node mesh.

~~~
apiVersion: projectcalico.org/v3
kind: BGPConfiguration
metadata:
  name: default
spec:
  asNumber: 63400
  logSeverityScreen: Info
  nodeToNodeMeshEnabled: false
~~~

Create the configuration:

~~~
calicoctl create -f calico-disable-global-peer.yaml
~~~

### Enable per node BGP peering (remote AS number)

It is possible to use a different AS numbers for different Calico nodes (Kubernetes nodes). If there is no additional configuration specified the global configuration will apply and will set the remote BGP peer AS number to `63400`. Change this by applying a `BGPPeer` resource configuration for the affected host.

*NOTE*: This will only change the **remote** peer AS number and **NOT** the local AS number of the Calico node agent!

~~~
apiVersion: projectcalico.org/v3
kind: BGPPeer
metadata:
  name: bgppeer-node-x4141
spec:
  peerIP: 10.x.x.140
  node: x4141
  asNumber: 63500
~~~

### Change the local AS number for a node

It is needed to change the local AS number of the Calico node too, to get a BGP peering with a different AS number peer up and running. Currently, there exist a small [bug](https://github.com/projectcalico/calico/issues/2021) but there is a workaround for it.

It is possible to change the local AS number of the Calico node by applying a `Node` resource configuration for the affected host.

*NOTE*: Only add the `asNumber` to the configuration! **Do not change anything else!**

~~~
apiVersion: projectcalico.org/v3
kind: Node
metadata:
  name: x4141
spec:
  bgp:
    asNumber: 63500
    ipv4Address: 10.x.x.141/20
    ipv4IPIPTunnelAddr: 192.168.161.64
  orchRefs:
  - nodeName: x4141
    orchestrator: k8s
~~~
