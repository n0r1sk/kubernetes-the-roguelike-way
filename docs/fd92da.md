# Operating System (depth 50 feet)
Most of the time we are using Ubuntu as core operating system. But feel free to contribute documentation for other operating systems too. If you have the experience on installing Kubernetes on Microsoft Windows, let us know!

![50feet](../img/67f3f1.jpg)
