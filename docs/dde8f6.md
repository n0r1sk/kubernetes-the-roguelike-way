# Docker

For the Docker installation on Ubuntu you can follow the following [official Docker installation on Ubuntu guide](https://docs.docker.com/install/linux/docker-ce/ubuntu/).

# Docker for Kubernetes
Install the correct Docker version according to the Kubernetes version. This information can be found in the release notes of the Kubernetes version.

For Kubernetes version `1.10` find the release notes [here](https://kubernetes.io/docs/imported/release/notes/). For Kubernetes `1.10` the minimum Docker version is `1.11` the highest version is `17.03`.

All version numbers of the external dependencies can be found in the changelog file of the Kubernetes release. For Kubernetes 1.10 you can [find it here](https://github.com/kubernetes/kubernetes/blob/master/CHANGELOG-1.10.md#external-dependencies).

# Important tips
- Use a specific Docker version
  - Eg `# apt-get install docker-ce=<VERSION>`, `# apt-get install docker-ce=17.12.1~ce-0~ubuntu`
- **apt-mark** the Docker package
  - This is **really** important, as otherwise the installed Docker engine will be automatically upgraded due to the Ubuntu system update!
  - `# apt-mark hold docker-ce`
