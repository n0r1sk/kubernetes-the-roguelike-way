# Certificates

For Kubernetes, a set of SSL certificates to secure most of the Kubernetes and Kubernetes related services is needed. The certificates are needed later during the installation and configuration of the various software components.

List of services that are using certificates:
- ETCD ((server and client) or peer)
- CoreDNS (client certificate to authenticate with ETCD)
- Kube-Bosnd (client certificate to authenticate with ETCD)

**Kubeadm will create a CA on it's own during the first run on the first master!** It is possible to create certificates for Kubernetes with another (own) CA too, but this is not part of this guide.

## SSL certificates using cfssl

### HowTo install & run cfssl
https://coreos.com/os/docs/latest/generate-self-signed-certificates.html

### Installation
```
mkdir ~/bin
curl -s -L -o ~/bin/cfssl https://pkg.cfssl.org/R1.2/cfssl_linux-amd64
curl -s -L -o ~/bin/cfssljson https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64
chmod +x ~/bin/{cfssl,cfssljson}
export PATH=$PATH:~/bin
```
### Configuration files used for cfssl
#### ca-csr.json
This is the CA signing request. Change the parameters as needed.
```
{
    "CN": "example.net",
    "hosts": [
        "example.net",
        "www.example.net"
    ],
    "key": {
        "algo": "ecdsa",
        "size": 256
    },
    "names": [
        {
            "C": "AT"
        }
    ]
}
```

#### ca-config.json
That is the CA configuration which specifies which types of certificates can be created. **server** is used for usual `https` server certificates. **client** is used for client authentication based on this CA and cannot be used as server certificate at the same time. Therefore there is the need for a **server** certificate *and* a **client** certificate for both, transport layer security *and* client authentication. Use the type **peer** as an alternative. This type will create a certificate which can be used as **server** *and* **client** certificate at the same time! In this guide we will use **peer** for the ETCD cluster as it reduces the complexity.
```
{
    "signing": {
        "default": {
            "expiry": "168h"
        },
        "profiles": {
            "server": {
                "expiry": "43800h",
                "usages": [
                    "signing",
                    "key encipherment",
                    "server auth"
                ]
            },
            "client": {
                "expiry": "43800h",
                "usages": [
                    "signing",
                    "key encipherment",
                    "client auth"
                ]
            },
            "peer": {
                "expiry": "43800h",
                "usages": [
                    "signing",
                    "key encipherment",
                    "server auth",
                    "client auth"
                ]
            }
        }
    }
}
```

### HowTo create a new Certificate Authority (CA)
The online documentation is really good, so have a look at it! This step is mandatory, as we need the CA to create the ETCD certificates.

`cfssl gencert -initca ca-csr.json | cfssljson -bare ca -`

### For general transport security (HTTPS)
**This is only a general example!**

`cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=server server.json | cfssljson -bare server`

### For peer/internal cluster communication
**This is only a general example!**

`cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=peer peer.json | cfssljson -bare peer`

### For client authentication
**This is only a general example!**

`cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=client client.json | cfssljson -bare client`
