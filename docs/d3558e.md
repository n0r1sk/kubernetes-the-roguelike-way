# BGP routing, Linux router with BGP route reflector

Calico node uses [BIRD](http://bird.network.cz/) as BGP daemon to configure the needed routing entries inside the Kubernetes host. This is perfect for the full node to node mesh but at some point the traffic has to be routed to and from the Kubernetes cluster. Usually a network router (in the cloud and on premise) should be able to talk the BGP protocol. But, if there is not BGP capable available or if there is the need for a lab test, it is possible to create a Linux based router which can talk with the BGP protocol.

For a detailed documentation on how to configure the Calico setting for a BGP look [here](9790cf.md).

## Linux as router

This setup uses Docker as local (unmanaged) container engine. It uses a `Dockerfile` and a small `ENTRYPOINT` script. The resulting image will be started with with special permissions. This might be not the ideal setup, but it a reasonable way. It uses BIRD as BGP software.

### Dockerfile

The `Dockerfile` is quite simple. It adds the `start.sh` script and installs iptables and BIRD as BGP implementation.

~~~
FROM ubuntu:16.04

ADD start.sh /start.sh

RUN apt-get update && \
    apt-get -y install bird iptables && \
    apt-get -y clean && \
    mkdir /run/bird && \
    chmod 755 /start.sh

CMD ["/start.sh"]
~~~

### startup.sh

The `startup.sh` script which is used as Docker entrypoint has four steps.
- Set the iptable rules to allow ip forwarding. Docker prohibits ip forwarding.
- Enable Kernel based ip forwarding.
- Disables ICMP redirects. In L2 networks their might be a shorter route to the target but with this setup we enforce the usage of the router.
- Start the bird daemon as bird user as forked process. This makes sure that bird is the first process in the container.

~~~
#!/bin/bash

# Allow ip forwarding via iptables
iptables -A FORWARD -i ens160 -j ACCEPT
iptables -A FORWARD -o ens160 -j ACCEPT

# Eanble ip formwarding
echo 1 > /proc/sys/net/ipv4/ip_forward

# Disable ICMP redirect
echo 0 | tee /proc/sys/net/ipv4/conf/*/send_redirects

# Start bird
bird -f -u bird -g bird
~~~

### Build the Docker image and start the container

Create the `Dockerfile` and the `startup.sh` and put them together in one director. Build the Docker image:

~~~
# docker build . -t bird:v1.0.0
~~~

Run the Docker image:

~~~
docker run -d --privileged --net=host --name=rr -v /root/bird/conf:/etc/bird bird:v1.0.0
~~~

### Check the output

Check the Docker logs output:
~~~
docker logs rr
0
2018-06-06 12:52:40 <TRACE> device1: Initializing
2018-06-06 12:52:40 <TRACE> kernel1: Initializing
2018-06-06 12:52:40 <TRACE> x4141: Initializing
2018-06-06 12:52:40 <TRACE> device1: Starting
2018-06-06 12:52:40 <TRACE> device1: Scanning interfaces
2018-06-06 12:52:40 <TRACE> device1: Connected to table master
2018-06-06 12:52:40 <TRACE> device1: State changed to feed
2018-06-06 12:52:40 <TRACE> kernel1: Starting
2018-06-06 12:52:40 <TRACE> kernel1: Connected to table master
2018-06-06 12:52:40 <TRACE> kernel1: State changed to feed
2018-06-06 12:52:40 <TRACE> x4141: Starting
2018-06-06 12:52:40 <TRACE> x4141: State changed to start
2018-06-06 12:52:40 <INFO> Started
2018-06-06 12:52:40 <TRACE> device1: State changed to up
2018-06-06 12:52:40 <TRACE> kernel1 < interface lo goes up
2018-06-06 12:52:40 <TRACE> kernel1 < interface ens160 goes up
2018-06-06 12:52:40 <TRACE> kernel1 < interface docker0 goes up
2018-06-06 12:52:40 <TRACE> kernel1: State changed to up
2018-06-06 12:52:40 <TRACE> x4141: Started
2018-06-06 12:52:40 <TRACE> x4141: Connect delayed by 5 seconds
2018-06-06 12:52:40 <TRACE> kernel1: Scanning routing table
2018-06-06 12:52:40 <TRACE> kernel1: 0.0.0.0/0: [alien] ignored
2018-06-06 12:52:40 <TRACE> kernel1: 192.168.161.64/26: will be removed
2018-06-06 12:52:40 <TRACE> kernel1: Pruning table master
2018-06-06 12:52:40 <TRACE> kernel1: 192.168.161.64/26: deleting
2018-06-06 12:52:40 <TRACE> x4141: Incoming connection from 10.x.x.141 (port 33432) accepted
2018-06-06 12:52:40 <TRACE> x4141: Sending OPEN(ver=4,as=63500,hold=240,id=0a49048c)
2018-06-06 12:52:40 <TRACE> x4141: Got OPEN(as=63500,hold=240,id=0a49048d)
2018-06-06 12:52:40 <TRACE> x4141: Sending KEEPALIVE
2018-06-06 12:52:40 <TRACE> x4141: Got KEEPALIVE
2018-06-06 12:52:40 <TRACE> x4141: BGP session established
2018-06-06 12:52:40 <TRACE> x4141: Connected to table master
2018-06-06 12:52:40 <TRACE> x4141: State changed to feed
2018-06-06 12:52:40 <TRACE> x4141: State changed to up
2018-06-06 12:52:40 <TRACE> x4141: Sending END-OF-RIB
2018-06-06 12:52:40 <TRACE> x4141: Got UPDATE
2018-06-06 12:52:40 <TRACE> x4141 > added [best] 192.168.161.64/26 via 10.x.x.141 on ens160
2018-06-06 12:52:40 <TRACE> kernel1 < added 192.168.161.64/26 via 10.x.x.141 on ens160
2018-06-06 12:52:40 <TRACE> x4141 < rejected by protocol 192.168.161.64/26 via 10.x.x.141 on ens160
2018-06-06 12:52:40 <TRACE> x4141: Got UPDATE
2018-06-06 12:52:40 <TRACE> x4141: Got END-OF-RIB
2018-06-06 12:52:43 <TRACE> device1: Scanning interfaces
2018-06-06 12:52:45 <TRACE> device1: Scanning interfaces
~~~

Check the local routes:
~~~
# ip route
default via 10.x.x.254 dev ens160 onlink 
10.73.0.0/20 dev ens160  proto kernel  scope link  src 10.x.x.140 
172.17.0.0/16 dev docker0  proto kernel  scope link  src 172.17.0.1 linkdown 
192.168.161.64/26 via 10.x.x.141 dev ens160  proto bird
~~~

Check process id of bird:
~~~
# docker exec -ti rr ps -ef
UID        PID  PPID  C STIME TTY          TIME CMD
bird         1     0  0 12:52 ?        00:00:00 bird -f -u bird -g bird
root        23     0  0 12:58 pts/0    00:00:00 ps -ef
~~~

### Change default gateway

After the setup of the Linux BGP route reflector, change the default gateway on the Kubernetes host to use the Linux BGP route reflector as default gateway. Otherwise the ip packets won't find their way to and from the Kubernetes Pods.
