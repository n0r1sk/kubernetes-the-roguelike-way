# Node label

To label a Kubernetes node use:
~~~
# kubectl label node x4141 dc=dc2
node "x4141" labeled
~~~

Label the node correctly, according to the Calico daemonset configuration. More detail can be found [here](../docs/9790cf.md).

After this step, the Calico/node pod on the correctly labeled Kubernetes node should come online.

Run the following to check if the Calico node is online.

~~~
NAME           ASN       IPV4               IPV6   
x26205   (63400)   10.x.x.205/16          
x26206   (63400)   10.x.x.206/16          
x26207   (63400)   10.x.x.207/16          
x26208   (63400)   10.x.x.208/16          
x26209   (63400)   10.x.x.209/16          
y4141    (63400)   10.y.y.141/20 
~~~

Check the content of the `/etc/cni/net.d/10-calico.conflist` file.

Also check the routes in the BIRD route reflector to now if the routes are there.

Be absolutely sure that all BGP neighbors are configured correctly as otherwise the peering will not work! Enable the debug output of the BIRD reflector for more insight on what is going on.

