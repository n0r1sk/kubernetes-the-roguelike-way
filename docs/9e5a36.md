# ETCD installation

There are many different ways on how to setup ETCD for the usage with Kubernetes:
- Integrated as **kublet** pods
- On the Kubernetes masters as Docker containers
- On the Kubernetes masters installed via the ETCD binary and controlled by systemd
- On external hosts (not on the Kubernetes masters) in one of the ways described above

In this guide the ETCD instances are **not** installed on the Kubernetes masters but on three different hosts using the Docker container engine installed there.

## Certificate

*NOTE:* The `ca.crt` was [created beforehand](1b2122.md).

#### etcd.json
```
{
    "CN": "etcd",
    "hosts": [
        "<ipaddress1>",
        "<ipaddress2>",
        "<ipaddress3>"
    ],
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "AT",
            "L": "ETCD"
        }
    ]
}

```

#### cfssl command
```
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=peer etcd.json | cfssljson -bare etcd
```
A warning message like this is OK:
```
2018/05/19 21:43:17 [WARNING] This certificate lacks a "hosts" field. This makes it unsuitable for
websites. For more information see the Baseline Requirements for the Issuance and Management
of Publicly-Trusted Certificates, v.1.1.6, from the CA/Browser Forum (https://cabforum.org);
specifically, section 10.2.3 ("Information Requirements").
```
Ignore this, [as it is an unfixed bug](https://github.com/cloudflare/cfssl/issues/717).

Check the resulting certificate with this command and check the SAN entries:
```
# cfssl certinfo -cert etcd.pem
...
  },
  "serial_number": "201256477541307782594429692616782582562871023485",
  "sans": [
    "10.x.x.205",
    "10.x.x.206",
    "127.0.0.1"
  ],
  "not_before": "2018-05-19T19:44:00Z",
  "not_after": "2023-05-18T19:44:00Z",
...
```
## Running ETCD
Run the ETCD instances on the three different hosts with the following command.

```
docker run -d --restart always --name etcd-$(hostname) --hostname etcd-$(hostname) \
  --net=host \
  -v /var/opt/etcd/data:/srv/data/etcd \
  -v /var/opt/etcd/certs:/certs \
  -v /var/opt/etcd/backup:/backup \
  quay.io/coreos/etcd:v3.3.2 \
    /usr/local/bin/etcd \
    --name etcd-$(hostname) \
    --peer-client-cert-auth \
    --peer-trusted-ca-file=/certs/ca.pem \
    --client-cert-auth \
    --trusted-ca-file=/certs/ca.pem \
    --cert-file=/certs/etcd.pem \
    --key-file=/certs/etcd-key.pem \
    --peer-cert-file=/certs/etcd.pem \
    --peer-key-file=/certs/etcd-key.pem \
    --initial-advertise-peer-urls https://<ipaddress1>:2380 \
    --listen-peer-urls https://<ipaddress1>:2380 \
    --listen-client-urls https://<ipaddress1>:2379 \
    --advertise-client-urls https://<ipaddress1>:2379 \
    --initial-cluster-token etcd-cluster \
    --initial-cluster etcd-$(hostname)=https://<ipaddress1>:2380,<etcd2>=https://<ipaddress2>:2380,<etcd3>=https://<ipaddress3>:2380 \
    --initial-cluster-state new \
    --data-dir /srv/data/etcd
```

#### Explanation:
This Docker command will run the ETCD container on the first host. Run this command on the additional two hosts to bring up the cluster. Some details about the command:

##### Volumes:
We mount three volumes from the host via bind mounts. `/var/opt/etcd/data` serves as persistance directory to persist the ETCD data. `/var/opt/etcd/certs` is the directory where the previously created certificates for ETCD are stored. `/var/opt/etcd/backup` is the backup folder.

##### ETCD configuration:
`etcd-$(hostname)` is used to give the three ETCD instanced an unique identifier. Replace the `<ip-addres-server*>` placeholders with the real ip addresses. Replace the `<etcd2> and <etcd3>` placeholders with the etcd naming of the second and the third ETCD host too. The rest of the configuration enables the SSL certificate (server and peer) and defines the listening addresses for the different ETCD service ports.

## Test ETCD
Test ETCD with this command. It will create a key named `foo` with a value of `bar`. The output would be simply `OK`. Be aware, that this command uses ETCD api version 3. It is not possible to retrieve keys that are stored in the api v2 path until the environment variable from the command is removed and the command itself is changed to a valid ETCD v2 query.

*NOTICE:* Kubernetes is already using the v3 ETCD api interface. CoreDNS not, it currently uses v2.

```
$ docker run --rm \
  -v /var/opt/etcd/certs:/certs \
  -e ETCDCTL_API=3 \
  quay.io/coreos/etcd:v3.3.2 \
  etcdctl \
    --cacert=/certs/ca.pem \
	--cert=/certs/etcd.pem \
	--key=/certs/etcd-key.pem \
	--endpoints=<ipaddress1>:2379 put /foo bar
```

```
OK
```


