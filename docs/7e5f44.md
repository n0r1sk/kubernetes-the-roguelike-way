# kube-bosnd Ingress load balancer / CoreDNS

Our [kube-bosnd](https://gitlab.com/n0r1sk/kube-bosnd) project supports the deployment of of a Nginx based Ingress load balancer.

Please look into the [kube-bosnd example documentation](https://gitlab.com/n0r1sk/kube-bosnd/blob/master/example/dashboard-kube-bosnd-lb-deployment.yaml). Basically, the deployment consists of the following parts (which is in one `yaml` file):

- Kubernetes Service Account to allow the reading of all Pods in the kube-system namespace
- Kubernetes Role and Rolebinding
- Kubernetes Configmap (This is important!)
- Kubernetes Deployment for the kube-bosnd Ingress
- Kubernetes Network Policies (ingress and egress)